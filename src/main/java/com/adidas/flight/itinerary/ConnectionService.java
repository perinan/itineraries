package com.adidas.flight.itinerary;

import java.util.List;

interface ConnectionService {

	public List<Connection> getConnections(String fromCity);

}
