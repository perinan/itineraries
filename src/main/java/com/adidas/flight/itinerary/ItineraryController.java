package com.adidas.flight.itinerary;

import org.modelmapper.ModelMapper;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@RestController
@RequestMapping("/best-itineraries")
@RequiredArgsConstructor
@Slf4j
class ItineraryController {

	private final ItineraryService service;

	private final ModelMapper modelMapper;

	@GetMapping("/from/{fromCity}/to/{toCity}")
	BestItinerariesDto getBestItineraries(@PathVariable(name = "fromCity", required = true) String fromCity,
			@PathVariable(name = "toCity", required = true) String toCity) {

		log.info(String.format("Best itineraries from '%s' to '%s'",
				fromCity,
				toCity));

		BestItineraries bestItineraries = service.getBestItineraries(fromCity,
				toCity);

		return modelMapper.map(bestItineraries,
				BestItinerariesDto.class);
	}
}
