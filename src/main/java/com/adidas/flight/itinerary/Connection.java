package com.adidas.flight.itinerary;

import java.time.LocalTime;

import lombok.Data;

@Data
class Connection {

	private String originCity;
	private String destinyCity;
	private LocalTime departureTime;
	private LocalTime arrivalTime;
	
}
