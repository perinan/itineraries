package com.adidas.flight.itinerary;

interface ItineraryService {
	
	BestItineraries getBestItineraries(String fromCity, String toCity);
}
