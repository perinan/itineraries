package com.adidas.flight.itinerary;

import java.util.List;

import org.springframework.stereotype.Service;

import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
class ItineraryServiceImpl implements ItineraryService {

	private final ConnectionService connectionService;

	@Override
	public BestItineraries getBestItineraries(String fromCity, String toCity) {
		List<Connection> initialConnection = connectionService.getConnections(fromCity);

		return BestItineraries.builder()
			.lessTime(initialConnection)
			.lessConnections(initialConnection)
			.build();
	}

}
