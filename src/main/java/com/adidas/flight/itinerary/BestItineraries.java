package com.adidas.flight.itinerary;

import java.util.List;

import lombok.Builder;
import lombok.Value;

@Value
@Builder
class BestItineraries {

	private final List<Connection> lessTime;
	private final List<Connection> lessConnections;

}
