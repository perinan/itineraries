package com.adidas.flight.itinerary;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;

import lombok.RequiredArgsConstructor;

@Component
@RequiredArgsConstructor
class ConnectionServiceClientRestImpl implements ConnectionService {

	private static final String END_POINT = "http://localhost:8081/connections/from";

	private final RestTemplate restTemplate;

	@SuppressWarnings("unchecked")
	@HystrixCommand(fallbackMethod = "defaultAnswer")
	public List<Connection> getConnections(String fromCity) {
		return restTemplate.getForObject(String.format("%s/%s", END_POINT, fromCity), List.class);
	}

	@SuppressWarnings("unused")
	private List<Connection> defaultAnswer (String fromCity) {
		return new ArrayList<>();
	}
}
