package com.adidas.flight.itinerary;

import java.util.List;

import lombok.Data;

@Data
class BestItinerariesDto {

	private List<Connection> lessTime;
	private List<Connection> lessConnections;
	
}
