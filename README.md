# Project Title

A Restful service that will consume the Connections API in order to calculate the sortest way (in time and in connections) to travel from one city to another, independent of the departure time.

## Getting Started

You can get a copy of the project with the following command:
 ```git clone https://bitbucket.org/perinan/itineraries.git```

### Prerequisites

Your machine has to have the following tools installed:
 - Maven
 - VM Java 8

### Build

You can build the project with the following command:
 ```mvn package```

## Run

You can run the microservice executing:
 ```mvn spring-boot:run```

## Use

You should open a browser and write the url ```http://localhost:8082/best-itineraries/from/{fromCity}/to/{toCity}```, where fromCity
is the origin city of the journey and the toCity the destiny city.

Example:
	```http://localhost:8082/best-itineraries/from/Seville/to/Madrid```

## Documentation

The documentation is a swagger site which is available in ```http://localhost:8082/swagger-ui.html```

## Frameworks and libraries used

This microservice has been developed using the following frameworks and libraries:
 - Java 8
 - Spring Boot.
 - Hystrix
 - Swagger.
 - Lombok.
